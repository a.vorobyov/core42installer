﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SetupDevEnvironment
{
    public static class SourceStorage
    {
        public static string GitUrl = "https://github.com/git-for-windows/git/releases/download/v2.24.0.windows.2/Git-2.24.0.2-64-bit.exe";
        public static string DockerUrl = "https://download.docker.com/win/stable/Docker%20Desktop%20Installer.exe";

        public static string MinikubeUrl =
            "https://github.com/kubernetes/minikube/releases/latest/download/minikube-installer.exe";
    }
}
