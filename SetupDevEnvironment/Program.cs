﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Management;
using System.Net;
using System.Net.Http.Headers;
using LibGit2Sharp;
using SetupDevEnvironment;
using Syroot.Windows.IO;
using System.Collections.ObjectModel;
//todo если директория с проекутом не пустая еррор - фиксь
namespace SetupDevEnvironment
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            //todo парсить cmd args 
            var installationPlan = new InstallationPlan(new List<IInstallable>
            {
                //new PuttyComponent(),                   //todo но сначала putty
                new GitComponent(), //todo делай установку гита
                new GetGitlabRepoComponent(),
                new MinikubeComponent(),
                new ClusterComponent()
            });

            var installationProcess = installationPlan.Start();
            try
            {
                installationProcess.Install();
            }
            catch (Exception e)
            {
                installationProcess.Uninstall();
            }
        }
    }


    internal class InstallationPlan
    {
        public IList<IInstallable> ComponentList { get; }

        public InstallationPlan(IList<IInstallable> componentList)
        {
            ComponentList = componentList;
        }

        internal InstallationProcess Start()
        {
            return new InstallationProcess(this);
        }
    }

    internal class InstallationProcess : IInstallable
    {
        //private InstallationPlan currentInstallationPlan;
        private Stack<IInstallable> installedComponents = new Stack<IInstallable>();
        private Queue<IInstallable> pendingComponents;

        public InstallationProcess(InstallationPlan installationPlan)
        {
            pendingComponents = new Queue<IInstallable>(installationPlan.ComponentList);
            //todo ининциализировать очередь компонентов к установке
        }

        public Exception Install()
        {
            while (pendingComponents.Count > 0)
            {
                var component = pendingComponents.Dequeue();
                try
                {
                    component.Install();

                }
                catch (Exception e)
                {
                    return e;
                }

                installedComponents.Push(component);
            }

            return null;
        }

        public Exception Uninstall()
        {

            while (installedComponents.Count > 0)
            {
                var component = installedComponents.Pop();
                try
                {
                    component.Uninstall();

                }
                catch (Exception e)
                {
                    return e;
                }
            }

            return null;
        }
    }

    public interface IInstallable
    {
        Exception Install();
        Exception Uninstall();
    }

    abstract class BaseComponent : IInstallable
    {
        public abstract bool Exists();

        public Exception Install()
        {
            return !Exists() ? DoInstall() : null;
        }

        protected virtual Exception DoInstall()
        {

            throw new NotImplementedException();
        }

        public Exception Uninstall()
        {
            return Exists() ? DoUninstall() : null;
        }

        protected virtual Exception DoUninstall()
        {
            throw new NotImplementedException();
        }
    }

    class FileInstaller : BaseComponent
    {
        public override bool Exists()
        {
            throw new NotImplementedException();
        }

        public void Install(string downloadUrl, string installPath, string fileName)
        {
            var iPath = string.Empty;
            var sb = new System.Text.StringBuilder();

            if (sb.Append(installPath[^1]).ToString() == @"\")
            {
                iPath = installPath + fileName;
            }
            else
            {
                iPath = installPath + @"\" + fileName;
            }

            using var client = new WebClient();
            client.DownloadFile(downloadUrl, iPath);

            var process = new Process
            {
                StartInfo = {FileName = iPath, WindowStyle = ProcessWindowStyle.Maximized}
            };
            // Configure the process using the StartInfo properties.
            process.Start();
            process.WaitForExit(); // Waits here for the process to exit.
        }

        public void Uninstall(string installPath, string fileName)
        {
            var iPath = string.Empty;
            var sb = new System.Text.StringBuilder();
            if (sb.Append(installPath[^1]).ToString() == @"\")
            {
                iPath = installPath + fileName;
            }
            else
            {
                iPath = installPath + @"\" + fileName;
            }

            System.IO.File.Delete(iPath);
        }
    }

    class GitComponent : BaseComponent
    {
        //var installPath = Environment.SpecialFolder.UserProfile + @"\Downloads";
        //todo: проверить, что установлен Git
        public override bool Exists()
        {
            var result = string.Empty;
            var procStartInfo =
                new System.Diagnostics.ProcessStartInfo("cmd", "/c where git.exe")
                {
                    RedirectStandardOutput = true,
                    UseShellExecute = false
                };
            using var proc = new Process
            {
                StartInfo = procStartInfo
            };
            proc.Start();
            result = proc.StandardOutput.ReadToEnd();
            return result.Contains("git.exe", StringComparison.CurrentCulture);

        }

        protected override Exception DoInstall()
        {
            var fileName = "gitInstaller.exe";
            var installPath = new KnownFolder(KnownFolderType.Downloads).Path;
            new FileInstaller().Install(SourceStorage.GitUrl, installPath, fileName);
            return null;
        }

        protected override Exception DoUninstall()
        {
            //todo unинсталлер
            return null;
        }
    }

    class PuttyComponent : BaseComponent  
    {
        public override bool Exists()
        {
            var procStartInfo =
                new System.Diagnostics.ProcessStartInfo("cmd", "/c where putty")
                {
                    RedirectStandardOutput = true,
                    UseShellExecute = false
                };
            using var proc = new Process
            {
                StartInfo = procStartInfo
            };
            proc.Start();
            var result = proc.StandardOutput.ReadToEnd();
            return result.Contains("putty.exe", StringComparison.CurrentCulture);
        }

        protected override Exception DoInstall()
        {
            return null;
        }

        protected override Exception DoUninstall()
        {
            return null;
        }
    }

    class GetGitlabRepoComponent : BaseComponent
    {
        public override bool Exists()
        {
            return false;
        }
        //DoInstall

        protected override Exception DoInstall()
        {
            Console.WriteLine("installPath:");
            string installRepoPath = Console.ReadLine();
            Console.WriteLine("username:");
            string username = Console.ReadLine();
            Console.WriteLine("pass:");
            string password = Console.ReadLine();

            var dirObject = $"win32_Directory.Name='{installRepoPath}'";
            using ManagementObject dir = new ManagementObject(dirObject);
            dir.Get();
            var outParams = dir.InvokeMethod("Delete", null,
                null);

            using var repo = new Repository();
            var cloneOptions = new CloneOptions
            {
                CredentialsProvider = (url, user, cred) => new UsernamePasswordCredentials()
                {
                    Username = username,
                    Password = password
                }
            }; //todo: поменять путь к репозиторию
            Repository.Clone("https://gitlab.com/a.vorobyov/diffiehellman", installRepoPath, cloneOptions);
            return null;
        }

        protected override Exception DoUninstall()
        {
            //todo unинсталлер
            return null;
        }

    }

    

    class ClusterComponent : BaseComponent
    {
        public const string DockerScriptFile = "CreateCluster.ps1";

        public override bool Exists()
        {
            return false;
        }

        protected override Exception DoInstall()
        {
            var cmd = $"/c powershell.exe -file {DockerScriptFile}";
            System.Diagnostics.Process.Start("cmd.exe", cmd);
            return null;
        }

        protected override Exception DoUninstall()
        {
            return null;
        }
    }

    class MinikubeComponent : BaseComponent
    {
        public override bool Exists()
        {

            return false;
            //var result = string.Empty;
            //var procStartInfo =
            //    new System.Diagnostics.ProcessStartInfo("cmd", "/c where minikube.exe")
            //    {
            //        RedirectStandardOutput = true,
            //        UseShellExecute = false
            //    };
            //using var proc = new Process
            //{
            //    StartInfo = procStartInfo
            //};
            //proc.Start();
            //result = proc.StandardOutput.ReadToEnd();
            //return result.Contains("minikube.exe", StringComparison.CurrentCulture);
        }

        protected override Exception DoInstall()
        {
            var installPath = new KnownFolder(KnownFolderType.Downloads).Path;
            var fileName = "minikubeInstall.exe";
            new FileInstaller().Install(SourceStorage.MinikubeUrl, installPath, fileName);
            return null;
        }
    }
}
//todo: Если компонент был устаеновлен - не делать uninstall, Если мы поставили - то uninstall  


//        // установка репозитория гитлаб
//        Console.WriteLine("Enter path to install core42 repository");
//        var installPath = Console.ReadLine();
//        var doRetry = false;
//        do
//        {
//            Console.WriteLine("Enter gitlab username or email");
//            var username = Console.ReadLine();

//            Console.WriteLine("Enter gitlab password");
//            var password = Console.ReadLine();

//            if (Directory.Exists(installPath))
//            {
//                try
//                {
//                    if (System.IO.Directory.GetDirectories(installPath).Length +
//                        System.IO.Directory.GetFiles(installPath).Length > 0)
//                    {
//                        new GetGitlabRepo().CleanDirectory(installPath);
//                        new GetGitlabRepo().DownloadRepo(installPath, username, password);
//                    }
//                    else
//                    {
//                        new GetGitlabRepo().DownloadRepo(installPath, username, password);
//                    }
//                }
//                catch
//                {
//                    Console.WriteLine("Wrong username or password");
//                    doRetry = true;
//                }
//            }
//            else
//            {
//                Directory.CreateDirectory(installPath);
//                {
//                    new GetGitlabRepo().DownloadRepo(installPath, username, password);
//                }
//            }

//        } while (doRetry);

//        Console.WriteLine("Path for Docker installing:");
//        var installDockerPath= Console.ReadLine();
//        new InstallDocker().DockerInstall(installDockerPath);

//        Console.WriteLine("Is Kubernetes enabled y/n:");
//        var proceed = Console.ReadLine();
//        if (proceed == "y")
//        {
//            new Cluster().Create();
//        }
//    }
//}

//class DockerComponent : BaseComponent
//{
//    public override bool Exists()
//    {
//        var procStartInfo =
//            new System.Diagnostics.ProcessStartInfo("cmd", "/c where docker")
//            {
//                RedirectStandardOutput = true,
//                UseShellExecute = false
//            };
//        using var proc = new Process
//        {
//            StartInfo = procStartInfo
//        };
//        proc.Start();
//        var result = proc.StandardOutput.ReadToEnd();
//        return result.Contains("docker.exe", StringComparison.CurrentCulture);
//    }

//    protected override Exception DoInstall()
//    {
//        var installPath = new KnownFolder(KnownFolderType.Downloads).Path;
//        var fileName = "dockerInstall.exe";
//        new FileInstaller().Install(SourceStorage.DockerUrl, installPath, fileName);
//        return null;
//    }


//}